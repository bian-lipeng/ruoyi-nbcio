import request from '@/utils/request';
import { NoticeForm, NoticeQuery, NoticeVO } from './types';
import { AxiosPromise } from 'axios';
// 查询公告列表
export function listNotice(query: NoticeQuery): AxiosPromise<NoticeVO[]> {
  return request({
    url: '/system/notice/list',
    method: 'get',
    params: query
  });
}

// 查询公告详细
export function getNotice(noticeId: string | number): AxiosPromise<NoticeVO> {
  return request({
    url: '/system/notice/' + noticeId,
    method: 'get'
  });
}

// 新增公告
export function addNotice(data: NoticeForm) {
  return request({
    url: '/system/notice',
    method: 'post',
    data: data
  });
}

// 修改公告
export function updateNotice(data: NoticeForm) {
  return request({
    url: '/system/notice',
    method: 'put',
    data: data
  });
}

// 更新用户阅读公告状态
export function updateUserIdAndNotice(data: any) {
  return request({
    url: '/system/noticeSend/updateUserIdAndNotice',
    method: 'put',
    data: data
  })
}

// 获取系统消息
export function listByUser(query: any) {
  return request({
    url: '/system/notice/listByUser',
    method: 'get',
    params: query
  })
}

// 删除公告
export function delNotice(noticeId: string | number | Array<string | number>) {
  return request({
    url: '/system/notice/' + noticeId,
    method: 'delete'
  });
}

// 获取我的系统消息
export function getMyNoticeSend(data: any) {
  return request({
    url: '/system/noticeSend/getMyNoticeSend',
    method: 'post',
    data: data
  })
}
