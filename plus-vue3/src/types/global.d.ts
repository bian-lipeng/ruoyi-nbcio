import type { ComponentInternalInstance as ComponentInstance, PropType as VuePropType } from 'vue';

declare global {
  /** vue Instance */
  declare type ComponentInternalInstance = ComponentInstance;
  /**vue */
  declare type PropType<T> = VuePropType<T>;

  /**
   * 界面字段隐藏属性
   */
  declare interface FieldOption {
    key: number;
    label: string;
    visible: boolean;
    children?: Array<FieldOption>;
  }

  /**
   * 弹窗属性
   */
  declare interface DialogOption {
    /**
     * 弹窗标题
     */
    title?: string;
    /**
     * 是否显示
     */
    visible: boolean;
  }

  declare interface UploadOption {
    /** 设置上传的请求头部 */
    headers: { [key: string]: any };

    /** 上传的地址 */
    url: string;
  }

  /**
   * 导入属性
   */
  declare interface ImportOption extends UploadOption {
    /** 是否显示弹出层 */
    open: boolean;
    /** 弹出层标题 */
    title: string;
    /** 是否禁用上传 */
    isUploading: boolean;

    /** 其他参数 */
    [key: string]: any;
  }
  /**
   * 字典数据  数据配置
   */
  declare interface DictDataOption {
    label: string;
    value: string;
    elTagType?: ElTagType;
    elTagClass?: string;
  }

  declare interface BaseEntity {
    createBy?: any;
    createDept?: any;
    createTime?: string;
    updateBy?: any;
    updateTime?: any;
  }

  /**
   * 分页数据
   * T : 表单数据
   * D : 查询参数
   */
  declare interface PageData<T, D> {
    form: T;
    queryParams: D;
    rules: ElFormRules;
  }
  /**
   * 分页查询参数
   */
  declare interface PageQuery {
    pageNum: number;
    pageSize: number;
  }
  /**
   * 流程业务扩展对象
   */
  declare interface WfMyBusiness {
    /**
     * 主键ID
     */
    id: string | number;
    /**
     * 流程定义key 一个key会有多个版本的id
     */
    processDefinitionKey: string;
    /**
     * 流程定义id 一个流程定义唯一
     */
    processDefinitionId: string;
    /**
     * 流程业务实例id 一个流程业务唯一，本表中也唯一
     */
    processInstanceId: string;
    /**
     * 流程业务简要描述
     */
    title: string;
    /**
     * 业务表id，理论唯一
     */
    dataId: string;
    /**
     * 业务类名，用来获取spring容器里的服务对象
     */
    serviceImplName: string;
    /**
     * 申请人
     */
    proposer: string;
    /**
     * 流程状态说明，有：启动  撤回  驳回  审批中  审批通过  审批异常
     */
    actStatus: string;
    /**
     * 当前的节点定义上的Id,
     */
    taskId: string;
    /**
     * 当前的节点
     */
    taskName: string;
    /**
     * 当前的节点实例上的Id
     */
    taskNameId: string;
    /**
     * 当前的节点可以处理的用户名
     */
    todoUsers: string;
    /**
     * 处理过的人
     */
    doneUsers: string;
    /**
     * 当前任务节点的优先级 流程定义的时候所填
     */
    priority: string;
    /**
     * 前端页面显示的路由地址
     */
    routeName: string;

    /**流程变量Map<String,Object>*/
    values: any;

    /**
     * 流程实例主键
     */
    deployId: string;
  }
}
export {};
