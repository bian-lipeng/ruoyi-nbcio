export const useFlowable = () => {
  const allFormComponent = computed(() => {
    return [
        {
          text:'单表示例',
          routeName: '@/views/workflow/demo/wf',
          component: markRaw(defineAsyncComponent( () => import('@/views/workflow/demo/wf'))),
          businessTable:'wf_demo'
        },
        /*{
          text:'主子表示例',
          routeName:'@/views/workflow/demo/modules/CesOrderMainForm',
          component:() => markRaw(defineAsyncComponent(import(`@/views/workflow/demo/modules/CesOrderMainForm`)),
          businessTable:'ces_order_main'
        }*/
    ]
  })
  const getFormComponent = (routeName) => {
    return allFormComponent.value.find((item) => item.routeName === routeName) || {}
  }
  return {
    allFormComponent,
    getFormComponent
  }
}
