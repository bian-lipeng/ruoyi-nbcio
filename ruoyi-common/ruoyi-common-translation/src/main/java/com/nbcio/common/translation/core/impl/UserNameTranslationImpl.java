package com.nbcio.common.translation.core.impl;

import com.nbcio.common.core.service.UserService;
import com.nbcio.common.translation.annotation.TranslationType;
import com.nbcio.common.translation.constant.TransConstant;
import com.nbcio.common.translation.core.TranslationInterface;

import lombok.AllArgsConstructor;

/**
 * 用户名翻译实现
 *
 * @author nbacheng
 */
@AllArgsConstructor
@TranslationType(type = TransConstant.USER_ID_TO_NAME)
public class UserNameTranslationImpl implements TranslationInterface<String> {

    private final UserService userService;

    @Override
    public String translation(Object key, String other) {
        if (key instanceof Long id) {
            return userService.selectUserNameById(id);
        }
        return null;
    }
}
