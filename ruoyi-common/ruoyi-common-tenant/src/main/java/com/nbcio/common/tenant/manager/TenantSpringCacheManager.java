package com.nbcio.common.tenant.manager;

import org.springframework.cache.Cache;

import com.nbcio.common.core.constant.GlobalConstants;
import com.nbcio.common.core.utils.StringUtils;
import com.nbcio.common.redis.manager.PlusSpringCacheManager;
import com.nbcio.common.tenant.helper.TenantHelper;

/**
 * 重写 cacheName 处理方法 支持多租户
 *
 * @author nbacheng
 */
public class TenantSpringCacheManager extends PlusSpringCacheManager {

    public TenantSpringCacheManager() {
    }

    @Override
    public Cache getCache(String name) {
        if (StringUtils.contains(name, GlobalConstants.GLOBAL_REDIS_KEY)) {
            return super.getCache(name);
        }
        String tenantId = TenantHelper.getTenantId();
        if (StringUtils.startsWith(name, tenantId)) {
            // 如果存在则直接返回
            return super.getCache(name);
        }
        return super.getCache(tenantId + ":" + name);
    }

}
