package com.nbcio.common.tenant.exception;

import java.io.Serial;

import com.nbcio.common.core.exception.base.BaseException;

/**
 * 租户异常类
 *
 * @author nbacheng
 */
public class TenantException extends BaseException {

    @Serial
    private static final long serialVersionUID = 1L;

    public TenantException(String code, Object... args) {
        super("tenant", code, args, null);
    }
}
