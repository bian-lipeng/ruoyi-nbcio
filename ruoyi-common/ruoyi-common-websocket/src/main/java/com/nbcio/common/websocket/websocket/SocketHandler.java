package com.nbcio.common.websocket.websocket;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.nbcio.common.core.base.BaseMap;
import com.nbcio.common.core.constant.CommonSendStatus;
import com.nbcio.common.redis.listener.NbcioRedisListener;

import cn.hutool.core.util.ObjectUtil;
import lombok.extern.slf4j.Slf4j;

/**
 * 监听消息(采用redis发布订阅方式发送消息)
 */
@Slf4j
@Component
public class SocketHandler implements NbcioRedisListener {

    @Autowired
    private WebSocketServer webSocket;

    @Override
    public void onMessage(BaseMap map) {
        log.info("【SocketHandler消息】Redis Listerer:" + map.toString());

        String userName= map.get("userName");
        String message = map.get("message");
        if (ObjectUtil.isNotEmpty(userName)) {
            webSocket.pushMessage(userName, message);
            //app端消息推送
            webSocket.pushMessage(userName+CommonSendStatus.APP_SESSION_SUFFIX, message);
        } else {
            webSocket.pushMessage(message);
        }

    }
}