package com.nbcio.common.core.service;

/**
 * 通用 角色服务
 *
 * @author nbacheng
 */
public interface RoleService {

    /**
     * 通过角色ID查询角色名称
     *
     * @param roleId 角色ID
     * @return 角色名称
     */
    String selectRoleNameById(Long roleId);
}
