package com.nbcio.demo.mapper;

import com.nbcio.common.mybatis.core.mapper.BaseMapperPlus;
import com.nbcio.demo.domain.TestDemoEncrypt;

/**
 * 测试加密功能
 *
 * @author nbacheng
 */
public interface TestDemoEncryptMapper extends BaseMapperPlus<TestDemoEncrypt, TestDemoEncrypt> {

}
