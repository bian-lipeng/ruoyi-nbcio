package com.nbcio.demo.mapper;

import com.nbcio.common.mybatis.annotation.DataColumn;
import com.nbcio.common.mybatis.annotation.DataPermission;
import com.nbcio.common.mybatis.core.mapper.BaseMapperPlus;
import com.nbcio.demo.domain.TestTree;
import com.nbcio.demo.domain.vo.TestTreeVo;

/**
 * 测试树表Mapper接口
 *
 * @author nbacheng
 * @date 2021-07-26
 */
@DataPermission({
    @DataColumn(key = "deptName", value = "dept_id"),
    @DataColumn(key = "userName", value = "user_id")
})
public interface TestTreeMapper extends BaseMapperPlus<TestTree, TestTreeVo> {

}
