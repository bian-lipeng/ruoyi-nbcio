package com.nbcio.system.mapper;

import com.nbcio.common.mybatis.core.mapper.BaseMapperPlus;
import com.nbcio.system.domain.SysLogininfor;
import com.nbcio.system.domain.vo.SysLogininforVo;

/**
 * 系统访问日志情况信息 数据层
 *
 * @author nbacheng
 */
public interface SysLogininforMapper extends BaseMapperPlus<SysLogininfor, SysLogininforVo> {

}
