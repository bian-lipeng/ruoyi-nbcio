package com.nbcio.system.mapper;

import com.nbcio.common.mybatis.core.mapper.BaseMapperPlus;
import com.nbcio.system.domain.SysConfig;
import com.nbcio.system.domain.vo.SysConfigVo;

/**
 * 参数配置 数据层
 *
 * @author nbacheng
 */
public interface SysConfigMapper extends BaseMapperPlus<SysConfig, SysConfigVo> {

}
