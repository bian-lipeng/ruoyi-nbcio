package com.nbcio.system.mapper;

import com.nbcio.common.mybatis.core.mapper.BaseMapperPlus;
import com.nbcio.system.domain.SysOperLog;
import com.nbcio.system.domain.vo.SysOperLogVo;

/**
 * 操作日志 数据层
 *
 * @author nbacheng
 */
public interface SysOperLogMapper extends BaseMapperPlus<SysOperLog, SysOperLogVo> {

}
