package com.nbcio.system.mapper;

import com.nbcio.common.mybatis.core.mapper.BaseMapperPlus;
import com.nbcio.system.domain.SysDictType;
import com.nbcio.system.domain.vo.SysDictTypeVo;

/**
 * 字典表 数据层
 *
 * @author nbacheng
 */
public interface SysDictTypeMapper extends BaseMapperPlus<SysDictType, SysDictTypeVo> {

}
