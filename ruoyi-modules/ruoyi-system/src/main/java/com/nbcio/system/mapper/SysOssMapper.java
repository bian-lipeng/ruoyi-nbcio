package com.nbcio.system.mapper;

import com.nbcio.common.mybatis.core.mapper.BaseMapperPlus;
import com.nbcio.system.domain.SysOss;
import com.nbcio.system.domain.vo.SysOssVo;

/**
 * 文件上传 数据层
 *
 * @author nbacheng
 */
public interface SysOssMapper extends BaseMapperPlus<SysOss, SysOssVo> {
}
