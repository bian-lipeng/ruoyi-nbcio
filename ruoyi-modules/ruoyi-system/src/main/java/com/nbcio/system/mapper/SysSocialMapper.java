package com.nbcio.system.mapper;

import com.nbcio.common.mybatis.core.mapper.BaseMapperPlus;
import com.nbcio.system.domain.SysSocial;
import com.nbcio.system.domain.vo.SysSocialVo;

/**
 * 社会化关系Mapper接口
 *
 * @author thiszhc
 */
public interface SysSocialMapper extends BaseMapperPlus<SysSocial, SysSocialVo> {

}
