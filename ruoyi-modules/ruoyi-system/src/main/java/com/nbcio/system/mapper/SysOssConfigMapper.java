package com.nbcio.system.mapper;

import com.nbcio.common.mybatis.core.mapper.BaseMapperPlus;
import com.nbcio.system.domain.SysOssConfig;
import com.nbcio.system.domain.vo.SysOssConfigVo;

/**
 * 对象存储配置Mapper接口
 *
 * @author nbacheng
 * @author 孤舟烟雨
 * @date 2021-08-13
 */
public interface SysOssConfigMapper extends BaseMapperPlus<SysOssConfig, SysOssConfigVo> {

}
