package com.nbcio.system.mapper;

import com.nbcio.common.mybatis.core.mapper.BaseMapperPlus;
import com.nbcio.system.domain.SysUserPost;

/**
 * 用户与岗位关联表 数据层
 *
 * @author nbacheng
 */
public interface SysUserPostMapper extends BaseMapperPlus<SysUserPost, SysUserPost> {

}
