package com.nbcio.system.controller.system;

import cn.dev33.satoken.secure.BCrypt;
import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.io.FileUtil;
import lombok.RequiredArgsConstructor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import com.nbcio.common.core.config.RuoYiConfig;
import com.nbcio.common.core.constant.Constants;
import com.nbcio.common.core.domain.R;
import com.nbcio.common.core.utils.StringUtils;
import com.nbcio.common.core.utils.file.FileUploadUtils;
import com.nbcio.common.core.utils.file.MimeTypeUtils;
import com.nbcio.common.encrypt.annotation.ApiEncrypt;
import com.nbcio.common.log.annotation.Log;
import com.nbcio.common.log.enums.BusinessType;
import com.nbcio.common.satoken.utils.LoginHelper;
import com.nbcio.common.web.core.BaseController;
import com.nbcio.system.domain.SysUser;
import com.nbcio.system.domain.bo.SysUserBo;
import com.nbcio.system.domain.bo.SysUserPasswordBo;
import com.nbcio.system.domain.bo.SysUserProfileBo;
import com.nbcio.system.domain.vo.AvatarVo;
import com.nbcio.system.domain.vo.ProfileVo;
import com.nbcio.system.domain.vo.SysOssVo;
import com.nbcio.system.domain.vo.SysUserVo;
import com.nbcio.system.service.CommonService;
import com.nbcio.system.service.ISysOssService;
import com.nbcio.system.service.ISysUserService;
import com.nbcio.common.core.domain.model.LoginUser;

import java.io.IOException;
import java.util.Arrays;

/**
 * 个人信息 业务处理
 *
 * @author nbacheng
 */
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/system/user/profile")
public class SysProfileController extends BaseController {

	@Autowired CommonService commonService; 
	
    private final ISysUserService userService;
    private final ISysOssService ossService;
    @Value(value = "${ruoyi.profile}")
    private String uploadpath;
    
    @Value(value = "${nbcio.localfilehttp}")
    private String localfilehttp;

    /**
     * 本地：local minio：minio 阿里：alioss
     */
    @Value(value="${ruoyi.uploadtype}")
    private String uploadtype;

    /**
     * 个人信息
     */
    @GetMapping
    public R<ProfileVo> profile() {
        SysUserVo user = userService.selectUserById(LoginHelper.getUserId());
        ProfileVo profileVo = new ProfileVo();
        profileVo.setUser(user);
        profileVo.setRoleGroup(userService.selectUserRoleGroup(user.getUserName()));
        profileVo.setPostGroup(userService.selectUserPostGroup(user.getUserName()));
        return R.ok(profileVo);
    }

    /**
     * 修改用户
     */
    @Log(title = "个人信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public R<Void> updateProfile(@RequestBody SysUserProfileBo profile) {
        SysUserBo user = BeanUtil.toBean(profile, SysUserBo.class);
        String username = LoginHelper.getUsername();
        if (StringUtils.isNotEmpty(user.getPhonenumber()) && !userService.checkPhoneUnique(user)) {
            return R.fail("修改用户'" + username + "'失败，手机号码已存在");
        }
        if (StringUtils.isNotEmpty(user.getEmail()) && !userService.checkEmailUnique(user)) {
            return R.fail("修改用户'" + username + "'失败，邮箱账号已存在");
        }
        user.setUserId(LoginHelper.getUserId());
        if (userService.updateUserProfile(user) > 0) {
            return R.ok();
        }
        return R.fail("修改个人信息异常，请联系管理员");
    }

    /**
     * 重置密码
     *
     * @param bo 新旧密码
     */
    @ApiEncrypt
    @Log(title = "个人信息", businessType = BusinessType.UPDATE)
    @PutMapping("/updatePwd")
    public R<Void> updatePwd(@Validated @RequestBody SysUserPasswordBo bo) {
        SysUserVo user = userService.selectUserById(LoginHelper.getUserId());
        String password = user.getPassword();
        if (!BCrypt.checkpw(bo.getOldPassword(), password)) {
            return R.fail("修改密码失败，旧密码错误");
        }
        if (BCrypt.checkpw(bo.getNewPassword(), password)) {
            return R.fail("新密码不能与旧密码相同");
        }

        if (userService.resetUserPwd(user.getUserId(), BCrypt.hashpw(bo.getNewPassword())) > 0) {
            return R.ok();
        }
        return R.fail("修改密码异常，请联系管理员");
    }

    /**
     * 头像上传
     *
     * @param avatarfile 用户头像
     */
    @Log(title = "用户头像", businessType = BusinessType.UPDATE)
    @PostMapping(value = "/avatar", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public R<AvatarVo> avatar(@RequestPart("avatarfile") MultipartFile avatarfile) {
        if (!avatarfile.isEmpty()) {
            String extension = FileUtil.extName(avatarfile.getOriginalFilename());
            if (!StringUtils.equalsAnyIgnoreCase(extension, MimeTypeUtils.IMAGE_EXTENSION)) {
                return R.fail("文件格式不正确，请上传" + Arrays.toString(MimeTypeUtils.IMAGE_EXTENSION) + "格式");
            }
            if(Constants.UPLOAD_TYPE_LOCAL.equals(uploadtype)) {
           	 // 上传文件路径
               String filePath = RuoYiConfig.getAvatarPath();
               // 上传并返回新文件名称
               String fileName = null;
				try {
					fileName = localfilehttp + FileUploadUtils.upload(filePath, avatarfile);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
               if (userService.updateUserAvatar(LoginHelper.getUserId(), fileName)) {
	                AvatarVo avatarVo = new AvatarVo();
            		avatarVo.setImgUrl(fileName);
            		return R.ok(avatarVo);
	            }
           }
            else {
            	SysOssVo oss = ossService.upload(avatarfile);
            	String avatar = oss.getUrl();
            	if (userService.updateUserAvatar(LoginHelper.getUserId(), avatar)) {
            		AvatarVo avatarVo = new AvatarVo();
            		avatarVo.setImgUrl(avatar);
            		return R.ok(avatarVo);
            	}
            }
            
        }
        return R.fail("上传图片异常，请联系管理员");
    }
}
