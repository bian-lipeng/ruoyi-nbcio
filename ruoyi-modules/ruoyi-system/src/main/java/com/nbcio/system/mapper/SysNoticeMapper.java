package com.nbcio.system.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.nbcio.common.mybatis.core.mapper.BaseMapperPlus;
import com.nbcio.system.domain.SysNotice;
import com.nbcio.system.domain.vo.SysNoticeVo;

/**
 * 通知公告表 数据层
 *
 * @author nbacheng
 */
public interface SysNoticeMapper extends BaseMapperPlus<SysNotice, SysNoticeVo> {

	List<SysNotice> querySysNoticeListByUserId(Page<SysNotice> page, @Param("userId")String userId,@Param("msgCategory")String msgCategory);

}
