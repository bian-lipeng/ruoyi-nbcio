package com.nbcio.system.mapper;

import com.nbcio.system.domain.SysClient;
import com.nbcio.system.domain.vo.SysClientVo;
import com.nbcio.common.mybatis.core.mapper.BaseMapperPlus;

/**
 * 授权管理Mapper接口
 *
 * @author Michelle.Chung
 * @date 2023-05-15
 */
public interface SysClientMapper extends BaseMapperPlus<SysClient, SysClientVo> {

}
