package com.nbcio.system.mapper;

import com.nbcio.common.mybatis.core.mapper.BaseMapperPlus;
import com.nbcio.system.domain.SysTenantPackage;
import com.nbcio.system.domain.vo.SysTenantPackageVo;

/**
 * 租户套餐Mapper接口
 *
 * @author Michelle.Chung
 */
public interface SysTenantPackageMapper extends BaseMapperPlus<SysTenantPackage, SysTenantPackageVo> {

}
