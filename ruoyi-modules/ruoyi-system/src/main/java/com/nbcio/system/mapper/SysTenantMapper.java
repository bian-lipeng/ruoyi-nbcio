package com.nbcio.system.mapper;

import com.nbcio.common.mybatis.core.mapper.BaseMapperPlus;
import com.nbcio.system.domain.SysTenant;
import com.nbcio.system.domain.vo.SysTenantVo;

/**
 * 租户Mapper接口
 *
 * @author Michelle.Chung
 */
public interface SysTenantMapper extends BaseMapperPlus<SysTenant, SysTenantVo> {

}
