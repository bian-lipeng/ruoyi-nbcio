package com.nbcio.system.controller.system;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import cn.dev33.satoken.annotation.SaCheckPermission;
import jakarta.annotation.Resource;
import lombok.RequiredArgsConstructor;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import com.nbcio.common.core.constant.Constants;
import com.nbcio.common.core.domain.R;
import com.nbcio.common.core.domain.model.LoginUser;
import com.nbcio.common.core.service.DictService;
import com.nbcio.common.log.annotation.Log;
import com.nbcio.common.log.enums.BusinessType;
import com.nbcio.common.mybatis.core.page.PageQuery;
import com.nbcio.common.mybatis.core.page.TableDataInfo;
import com.nbcio.common.web.core.BaseController;
import com.nbcio.common.websocket.utils.WebSocketUtils;
import com.nbcio.system.domain.SysNoticeSend;
import com.nbcio.system.domain.SysNotice;
import com.nbcio.system.domain.bo.SysNoticeBo;
import com.nbcio.system.domain.vo.SysNoticeVo;
import com.nbcio.system.service.CommonService;
import com.nbcio.system.service.ISysNoticeSendService;
import com.nbcio.system.service.ISysNoticeService;

/**
 * 公告 信息操作处理
 *
 * @author nbacheng
 */
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/system/notice")
public class SysNoticeController extends BaseController {

    private final ISysNoticeService noticeService;
    private final DictService dictService;
    private final ISysNoticeSendService noticeSendService;
    
    @Resource
    private CommonService commonService;


    /**
     * 获取通知公告列表
     */
    @SaCheckPermission("system:notice:list")
    @GetMapping("/list")
    public TableDataInfo<SysNoticeVo> list(SysNoticeBo notice, PageQuery pageQuery) {
        return noticeService.selectPageNoticeList(notice, pageQuery);
    }

    /**
     * 根据通知公告编号获取详细信息
     *
     * @param noticeId 公告ID
     */
    @SaCheckPermission("system:notice:query")
    @GetMapping(value = "/{noticeId}")
    public R<SysNoticeVo> getInfo(@PathVariable Long noticeId) {
        return R.ok(noticeService.selectNoticeById(noticeId));
    }

    /**
     * 新增通知公告
     */
    @SaCheckPermission("system:notice:add")
    @Log(title = "通知公告", businessType = BusinessType.INSERT)
    @PostMapping
    public R<Void> add(@Validated @RequestBody SysNoticeBo notice) {
        int rows = noticeService.insertNotice(notice);
        if (rows <= 0) {
            return R.fail();
        }
        String type = dictService.getDictLabel("sys_notice_type", notice.getNoticeType());
        WebSocketUtils.publishAll("[" + type + "] " + notice.getNoticeTitle());
        return R.ok();
    }

    /**
     * 修改通知公告
     */
    @SaCheckPermission("system:notice:edit")
    @Log(title = "通知公告", businessType = BusinessType.UPDATE)
    @PutMapping
    public R<Void> edit(@Validated @RequestBody SysNoticeBo notice) {
        return toAjax(noticeService.updateNotice(notice));
    }
    
    /**
	 * 补充用户数据，并返回系统消息
	 * @return
	 */
    @Log(title = "系统消息")
    @GetMapping("/listByUser")
    public R<Map<String, Object>> listByUser(@RequestParam(required = false, defaultValue = "5") Integer pageSize) {
    	LoginUser loginUser = commonService.getLoginUser();
		Long userId = loginUser.getUserId();
		// 1.将系统消息补充到用户通告阅读标记表中
		LambdaQueryWrapper<SysNotice> querySaWrapper = new LambdaQueryWrapper<SysNotice>();
		querySaWrapper.eq(SysNotice::getMsgType,Constants.MSG_TYPE_ALL); // 全部人员
		querySaWrapper.eq(SysNotice::getStatus,Constants.CLOSE_FLAG_0.toString());  // 未关闭
		querySaWrapper.eq(SysNotice::getSendStatus, Constants.HAS_SEND); //已发布
		//querySaWrapper.ge(SysNotice::getEndTime, loginUser.getCreateTime()); //新注册用户不看结束通知
		querySaWrapper.notInSql(SysNotice::getNoticeId,"select notice_id from sys_notice_send where user_id='"+userId+"'");
		List<SysNotice> notices = noticeService.list(querySaWrapper);
		if(notices.size()>0) {
			for(int i=0;i<notices.size();i++) {	
				//因为websocket没有判断是否存在这个用户，要是判断会出现问题，故在此判断逻辑
				LambdaQueryWrapper<SysNoticeSend> query = new LambdaQueryWrapper<>();
				query.eq(SysNoticeSend::getNoticeId,notices.get(i).getNoticeId());
				query.eq(SysNoticeSend::getUserId,userId);
				SysNoticeSend one = noticeSendService.getOne(query);
				if(null==one){
					SysNoticeSend noticeSend = new SysNoticeSend();
					noticeSend.setNoticeId(notices.get(i).getNoticeId());
					noticeSend.setUserId(userId);
					noticeSend.setReadFlag(Constants.NO_READ_FLAG);
					noticeSendService.save(noticeSend);
				}
			}
		}
		// 2.查询用户未读的系统消息
		Page<SysNotice> anntMsgList = new Page<SysNotice>(0, pageSize);
		anntMsgList = noticeService.querySysNoticePageByUserId(anntMsgList,userId,"1");//通知公告消息
		Page<SysNotice> sysMsgList = new Page<SysNotice>(0, pageSize);
		sysMsgList = noticeService.querySysNoticePageByUserId(sysMsgList,userId,"2");//系统消息
		Page<SysNotice> todealMsgList = new Page<SysNotice>(0, pageSize);
		todealMsgList = noticeService.querySysNoticePageByUserId(todealMsgList,userId,"3");//待办消息
		Map<String,Object> sysMsgMap = new HashMap<String, Object>();
		sysMsgMap.put("sysMsgList", sysMsgList.getRecords());
		sysMsgMap.put("sysMsgTotal", sysMsgList.getTotal());
		sysMsgMap.put("anntMsgList", anntMsgList.getRecords());
		sysMsgMap.put("anntMsgTotal", anntMsgList.getTotal());
		sysMsgMap.put("todealMsgList", todealMsgList.getRecords());
		sysMsgMap.put("todealMsgTotal", todealMsgList.getTotal());
		return R.ok(sysMsgMap);
    }

    /**
     * 删除通知公告
     *
     * @param noticeIds 公告ID串
     */
    @SaCheckPermission("system:notice:remove")
    @Log(title = "通知公告", businessType = BusinessType.DELETE)
    @DeleteMapping("/{noticeIds}")
    public R<Void> remove(@PathVariable Long[] noticeIds) {
        return toAjax(noticeService.deleteNoticeByIds(noticeIds));
    }
}
