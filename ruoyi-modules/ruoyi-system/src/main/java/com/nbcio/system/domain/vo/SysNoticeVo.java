package com.nbcio.system.domain.vo;

import com.nbcio.common.translation.annotation.Translation;
import com.nbcio.common.translation.constant.TransConstant;
import com.nbcio.system.domain.SysNotice;

import io.github.linpeilie.annotations.AutoMapper;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.util.Date;



/**
 * 通知公告视图对象 sys_notice
 *
 * @author Michelle.Chung,nbacheng
 */
@Data
@AutoMapper(target = SysNotice.class)
public class SysNoticeVo implements Serializable {

    @Serial
    private static final long serialVersionUID = 1L;

    /**
     * 公告ID
     */
    private Long noticeId;

    /**
     * 公告标题
     */
    private String noticeTitle;

    /**
     * 公告类型（1通知 2公告 3待办）
     */
    private String noticeType;

    /**
     * 公告内容
     */
    private String noticeContent;

    /**
     * 公告状态（0正常 1关闭）
     */
    private String status;
	
	/**
     * 发布人
     */
	private Long sender;
	
	/**
     * 优先级（L低，M中，H高）
     */
	private String priority;
	
	/**
     * 通告对象类型（USER:指定用户，ALL:全体用户）
     */
	private String msgType;
	
	/**
     * 发布状态（0未发布，1已发布，2已撤销）
     */
	private String sendStatus;

    /**
     * 发布时间
     */
	private Date sendTime;
	
	/**
     * 撤销时间
     */
	private Date cancelTime;
	 
    /**
     * 备注
     */
    private String remark;

    /**
     * 创建者
     */
    private Long createBy;

    /**
     * 创建人名称
     */
    @Translation(type = TransConstant.USER_ID_TO_NAME, mapper = "createBy")
    private String createByName;

    /**
     * 创建时间
     */
    private Date createTime;

}
