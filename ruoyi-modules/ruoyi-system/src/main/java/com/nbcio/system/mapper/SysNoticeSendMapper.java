package com.nbcio.system.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.nbcio.common.mybatis.core.mapper.BaseMapperPlus;
import com.nbcio.system.domain.SysNoticeSend;
import com.nbcio.system.domain.vo.SysNoticeSendVo;
import com.nbcio.system.model.NoticeSendModel;

/**
 * 用户公告阅读标记Mapper接口
 *
 * @author nbacheng
 * @date 2023-09-21
 */
public interface SysNoticeSendMapper extends BaseMapperPlus<SysNoticeSend, SysNoticeSendVo> {

	List<NoticeSendModel> getMyNoticeSendList(Page<NoticeSendModel> pageList, @Param("noticeSendModel")NoticeSendModel noticeSendModel);

}
