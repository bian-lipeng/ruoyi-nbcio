package com.nbcio.workflow.mapper;

import com.nbcio.common.mybatis.core.mapper.BaseMapperPlus;
import com.nbcio.workflow.domain.WfDeployForm;
import com.nbcio.workflow.domain.vo.WfDeployFormVo;

/**
 * 流程实例关联表单Mapper接口
 *
 * @author nbacheng
 * @createTime 2022/3/7 22:07
 */
public interface WfDeployFormMapper extends BaseMapperPlus<WfDeployForm, WfDeployFormVo> {

	WfDeployForm selectSysDeployFormByFormId(String id);

}
