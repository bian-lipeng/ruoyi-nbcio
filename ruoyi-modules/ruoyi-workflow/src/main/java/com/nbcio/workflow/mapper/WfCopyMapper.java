package com.nbcio.workflow.mapper;

import org.apache.ibatis.annotations.Param;

import com.nbcio.common.mybatis.core.mapper.BaseMapperPlus;
import com.nbcio.workflow.domain.WfCopy;
import com.nbcio.workflow.domain.vo.WfCopyVo;

/**
 * 流程抄送Mapper接口
 *
 * @author nbacheng
 * @date 2022-05-19
 */
public interface WfCopyMapper extends BaseMapperPlus<WfCopy, WfCopyVo> {
  public int updateState(@Param("id") String id);
}
