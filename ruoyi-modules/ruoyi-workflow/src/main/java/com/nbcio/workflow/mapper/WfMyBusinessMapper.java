package com.nbcio.workflow.mapper;

import com.nbcio.common.mybatis.core.mapper.BaseMapperPlus;
import com.nbcio.workflow.domain.WfMyBusiness;
import com.nbcio.workflow.domain.vo.WfMyBusinessVo;

/**
 * 流程业务扩展Mapper接口
 *
 * @author nbacheng
 * @date 2023-10-11
 */
public interface WfMyBusinessMapper extends BaseMapperPlus<WfMyBusiness, WfMyBusinessVo> {

}
