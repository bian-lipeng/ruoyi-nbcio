package com.nbcio.workflow.domain.vo;

import io.github.linpeilie.annotations.AutoMapper;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

import com.nbcio.workflow.domain.WfForm;

/**
 * 流程分类视图对象
 *
 * @author nbacheng
 * @createTime 2022/3/7 22:07
 */
@Data
@AutoMapper(target = WfForm.class)
public class WfFormVo implements Serializable {

    @Serial
    private static final long serialVersionUID = 1L;

    /**
     * 表单主键
     */
    private Long formId;

    /**
     * 表单名称
     */
    private String formName;

    /**
     * 表单内容
     */
    private String content;

    /**
     * 备注
     */
    private String remark;
    /**
     * 删除标志
     */
    private String delFlag;
    /**
     * 租户编号
     */
    //private String tenantId;
}
