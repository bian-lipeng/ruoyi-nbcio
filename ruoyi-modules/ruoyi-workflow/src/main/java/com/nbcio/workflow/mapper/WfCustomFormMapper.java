package com.nbcio.workflow.mapper;

import com.nbcio.common.mybatis.core.mapper.BaseMapperPlus;
import com.nbcio.workflow.domain.WfCustomForm;
import com.nbcio.workflow.domain.bo.WfCustomFormBo;
import com.nbcio.workflow.domain.vo.CustomFormVo;
import com.nbcio.workflow.domain.vo.WfCustomFormVo;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;


/**
 * 流程业务单Mapper接口
 *
 * @author nbacheng
 * @date 2023-10-09
 */
public interface WfCustomFormMapper extends BaseMapperPlus<WfCustomForm, WfCustomFormVo> {
	void updateCustom(@Param("customFormVo") CustomFormVo customFormVo);
	WfCustomForm selectSysCustomFormById(Long formId);
	List<WfCustomForm> selectSysCustomFormByServiceName(String serviceName);
	WfCustomFormBo selectSysCustomFormByDeployId(String deployId);
	@Select("select business_service from wf_custom_form where deploy_id = #{deployId}")
	String selectServiceNameByDeployId(@Param("deployId") String deployId);
}
