package com.nbcio.workflow.mapper;

import com.nbcio.common.mybatis.core.mapper.BaseMapperPlus;
import com.nbcio.workflow.domain.WfFlowConfig;
import com.nbcio.workflow.domain.vo.WfFlowConfigVo;

import org.apache.ibatis.annotations.Param;


/**
 * 流程配置主Mapper接口
 *
 * @author nbacheng
 * @date 2023-11-19
 */
public interface WfFlowConfigMapper extends BaseMapperPlus<WfFlowConfig, WfFlowConfigVo> {
	void updateFlowConfig(@Param("flowConfigVo") WfFlowConfigVo flowConfigVo);
	WfFlowConfig selectByModelIdAndNodeKey(@Param("flowConfigVo") WfFlowConfigVo flowConfigVo);
}
