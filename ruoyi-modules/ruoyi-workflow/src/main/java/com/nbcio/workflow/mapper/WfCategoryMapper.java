package com.nbcio.workflow.mapper;

import org.apache.ibatis.annotations.Param;

import com.nbcio.common.mybatis.core.mapper.BaseMapperPlus;
import com.nbcio.workflow.domain.WfCategory;
import com.nbcio.workflow.domain.vo.WfAppTypeVo;
import com.nbcio.workflow.domain.vo.WfCategoryVo;

/**
 * 流程分类Mapper接口
 *
 * @author nbacheng
 * @date 2022-01-15
 */
public interface WfCategoryMapper extends BaseMapperPlus<WfCategory, WfCategoryVo> {
   String selectAppTypeByCode(@Param("code") String code, @Param("tenantId") String tenantId);
   WfAppTypeVo selectAppTypeVoByCode(@Param("code") String code, @Param("tenantId") String tenantId);
}
