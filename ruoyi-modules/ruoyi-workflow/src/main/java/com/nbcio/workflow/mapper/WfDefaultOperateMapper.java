package com.nbcio.workflow.mapper;

import com.nbcio.common.mybatis.core.mapper.BaseMapperPlus;
import com.nbcio.workflow.domain.WfDefaultOperate;
import com.nbcio.workflow.domain.vo.WfDefaultOperateVo;

/**
 * 流程默认操作Mapper接口
 *
 * @author nbacheng
 * @date 2023-11-23
 */
public interface WfDefaultOperateMapper extends BaseMapperPlus<WfDefaultOperate, WfDefaultOperateVo> {

}
