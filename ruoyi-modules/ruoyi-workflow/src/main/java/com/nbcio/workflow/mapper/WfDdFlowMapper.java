package com.nbcio.workflow.mapper;

import com.nbcio.common.mybatis.core.mapper.BaseMapperPlus;
import com.nbcio.workflow.domain.WfDdFlow;
import com.nbcio.workflow.domain.vo.WfDdFlowVo;

/**
 * 钉钉流程Mapper接口
 *
 * @author nbacheng
 * @date 2023-11-29
 */
public interface WfDdFlowMapper extends BaseMapperPlus<WfDdFlow, WfDdFlowVo> {

}
