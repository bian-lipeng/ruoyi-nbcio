package com.nbcio;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.metrics.buffering.BufferingApplicationStartup;

/**
 * 启动程序
 *
 * @author nbacheng
 */

@SpringBootApplication
public class NbcioApplication {

    public static void main(String[] args) {
        SpringApplication application = new SpringApplication(NbcioApplication.class);
        application.setApplicationStartup(new BufferingApplicationStartup(2048));
        application.run(args);
        System.out.println("(♥◠‿◠)ﾉﾞ  RuoYi-Nbcio-Plus启动成功   ლ(´ڡ`ლ)ﾞ");
    }

}
